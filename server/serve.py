#!/usr/bin/env python3

from hupper import start_reloader
from sys import argv
from waitress import serve
from ghost_pwa_server import create_app

def main(args=argv[1:]):
    if '--reload' in args:
        reloader = hupper.start_reloader('myapp.scripts.serve.main')

    serve(create_app(), host='127.0.0.1')

if __name__ == '__main__':
    main()
