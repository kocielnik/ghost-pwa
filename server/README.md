# Ghost PWA Server

Run:

```bash
$ poetry run waitress-serve --host 127.0.0.1 \
    --call ghost_pwa_server:create_app
```

Run on production:

```bash
$ poetry run hupper -m waitress --host 127.0.0.1 \
    --call ghost_pwa_server:create_app
```

# Next steps

1. Remove each file after it is downloaded after processing [^remove_file].

[^remove_file]: https://stackoverflow.com/q/40853201
