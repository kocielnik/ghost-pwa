from flask import Blueprint, Flask, current_app
from get_secret_key import get_secret_key


def get_simple_page_blueprint():
    simple_page = Blueprint("simple_page", __name__)

    @simple_page.route('/')
    def index():
        return "Hello, world!"

    return simple_page

def create_app():
    simple_page = get_simple_page_blueprint()

    app = Flask(__name__)
    app.config.update(
        TESTING=True,
        DEBUG= True,
        FLASK_ENV='development',
        SECRET_KEY=get_secret_key()
    )
    app.register_blueprint(simple_page)

    return app
