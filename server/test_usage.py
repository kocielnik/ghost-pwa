#!/usr/bin/env python3

from os import getcwd, chdir
from os.path import expanduser
from pytest import skip
from requests import get, post

def test_local():
    print(getcwd())
    chdir(expanduser('~'))
    print(getcwd())

    import ghost_pwa

def test_remote():
    skip("Integration test")
    url = 'https://ghostpwa.com'
    data_file_path = 'test/Casper-5.4.5.zip'
    # https://codeload.github.com/TryGhost/Casper/zip/refs/tags/v5.4.5

    with open(data_file_path, mode='rb') as file:
        data = file.read()

    res = post(url, data={"file": data})

    print(res.history[0].content)
    print(res.history[1])
    print(res.content)

if __name__ == "__main__":
    #test_local()
    test_remote()
