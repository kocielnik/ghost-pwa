from flask import Flask, flash, request, redirect, url_for, send_from_directory
from flask import Blueprint, current_app
import os
import shutil
from werkzeug.utils import secure_filename
from tempfile import TemporaryDirectory

from get_secret_key import get_secret_key


def file_type_is_allowed(filename, allowed_types={'zip'}):
    """
    >>> file_type_is_allowed("x.zip")
    True
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in allowed_types

def process_upload(request, uploads_dir):
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part in the request.')
        return redirect(request.url)
    file = request.files['file']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        flash('No file selected.')
        return redirect(request.url)
    if file and file_type_is_allowed(file.filename):
        filename = secure_filename(file.filename)
        out_file = os.path.join(uploads_dir, filename)
        file.save(out_file)
        new_file = transform_file(uploads_dir, filename)

        try:
            shutil.move(new_file, uploads_dir)
        except shutil.Error as e:
            print(f"{e}, skipping.")

        return redirect(url_for('web_ui.download_file', name=new_file))

class Config:
    VERSION = "1.1.0"
    MODULE_DIR = os.path.dirname(os.path.realpath(__file__))
    UPLOAD_FOLDER = TemporaryDirectory()
    SECRET_KEY=get_secret_key()

def transform_file(folder, filename):
    from ghost_pwa.ghost_pwa import (get_file, make_pwa)

    in_file = os.path.join(folder, filename)
    make_pwa(in_file)
    new_filename = f"{filename[:-4]}-PWA.zip"
    return new_filename

def get_web_ui_blueprint(config):
    web_ui = Blueprint("web_ui", __name__)

    @web_ui.route('/')
    def main():
        return send_from_directory(config.MODULE_DIR, "index.html")

    @web_ui.route('/', methods=['POST'])
    def upload_file():
        return process_upload(request, config.UPLOAD_FOLDER.name)

    @web_ui.route('/uploads/<name>')
    def download_file(name):
        return send_from_directory(config.UPLOAD_FOLDER.name, name)

    return web_ui

def create_app():
    config = Config()

    print(f"Version: {config.VERSION}")
    print(f"Uploads dir: {config.UPLOAD_FOLDER.name}")

    web_ui_blueprint = get_web_ui_blueprint(config)

    app = Flask(__name__)

    app.register_blueprint(web_ui_blueprint)

    app.secret_key=config.SECRET_KEY

    return app
