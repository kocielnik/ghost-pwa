from pwa import create_app
import ghost_pwa_server


def test_home_page():
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    flask_app = create_app()

    with flask_app.test_client() as test_client:
        response = test_client.get('/')
        assert response.status_code == 200
        assert b"Hello, world!" in response.data

def test_ghost_pwa_server_main():
    flask_app = ghost_pwa_server.create_app()

    with flask_app.test_client() as test_client:
        response = test_client.get('/')
        assert response.status_code == 200
        assert b"Upload a theme" in response.data

def test_ghost_pwa_server_upload():
    flask_app = ghost_pwa_server.create_app()

    with flask_app.test_client() as test_client:
        response = test_client.post('/')
        assert response.status_code == 302
        assert b"Redirecting" in response.data
