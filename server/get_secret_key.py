import secrets


def get_secret_key():
    return secrets.token_hex()
