packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = var.docker_image
  commit = true
}

build {
  name = "kocielnik/ghost-pwa"
  sources = [
    "source.docker.ubuntu",
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      "echo Adding file to Docker container",
      "echo \"FOO is $FOO\" > example.txt",
    ]
  }

  provisioner "shell" {
    inline = ["echo Running ${var.docker_image} Docker image."]
  }

  provisioner "shell" {
    inline = [
      "export DEBIAN_FRONTEND=noninteractive",
      "apt-get update",
      "apt-get install -y apt-utils",
      "apt-get install -y git python3 curl",
      "apt-get clean",
      "curl -sSL https://install.python-poetry.org | python3 -",
      "git clone https://gitlab.com/kocielnik/ghost-pwa.git"
    ]
  }

  post-processor "docker-tag" {
    repository = "kocielnik/ghost-pwa"
    tags = ["latest"]
    only = ["docker.ubuntu"]
  }
}

variable "docker_image" {
  type = string
  default = "ubuntu:jammy"
}
