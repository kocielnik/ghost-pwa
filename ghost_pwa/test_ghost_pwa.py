from zipfile import Path as ZipPath, ZipFile
from pathlib import Path
from os import remove
from ghost_pwa.ghost_pwa import get_processed_zip, ExampleArchive, mkdir

def test_usage():
    from os.path import isfile as file_exists
    from tempfile import TemporaryDirectory
    from ghost_pwa.ghost_pwa import (get_file, make_pwa)

    with TemporaryDirectory() as temp_dir:
        get_file(
            "https://codeload.github.com/TryGhost/Casper/zip/refs/tags/3.1.3",
            f"{temp_dir}/Casper-3.1.3.zip"
        )
        make_pwa(f"{temp_dir}/Casper-3.1.3.zip")
        assert file_exists(f"Casper-3.1.3-PWA.zip")

def test_update_hbs_head():
    from ghost_pwa.ghost_pwa import header_insert, insert_before_tag

    original = """
    {{!-- Theme assets - use the {asset} helper to reference styles & scripts,
    this will take care of caching and cache-busting automatically --}}
    <link rel="stylesheet" type="text/css" href="{{asset "built/screen.css"}}" />

    {{!-- This tag outputs all your advanced SEO meta, structured data, and other important settings,
    it should always be the last tag before the closing head tag --}}
    {{ghost_head}}
    """

    expected = """
    {{!-- Theme assets - use the {asset} helper to reference styles & scripts,
    this will take care of caching and cache-busting automatically --}}
    <link rel="stylesheet" type="text/css" href="{{asset "built/screen.css"}}" />

    {{!-- PWA --}}
    <link rel="manifest" href="/manifest.webmanifest">
    <meta name="theme-color" content="#0075FF">
    <link rel="apple-touch-icon" href="/assets/icons/ghost-4.png">

    {{!-- This tag outputs all your advanced SEO meta, structured data, and other important settings,
    it should always be the last tag before the closing head tag --}}
    {{ghost_head}}
    """

    current = insert_before_tag("ghost_head", header_insert, original)

    assert current == expected

def test_update_hbs_foot():
    from ghost_pwa.ghost_pwa import footer_insert, insert_before_tag

    original = """
<script>
(...)
</script>

{{!-- Ghost outputs (...) --}}
{{ghost_foot}}
    """

    expected = """
<script>
(...)
</script>

{{!-- Service Worker for PWA --}}
<script>
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
    navigator.serviceWorker.register('/sw.js');
    });
}
</script>

{{!-- Ghost outputs (...) --}}
{{ghost_foot}}
    """

    current = insert_before_tag("ghost_foot", footer_insert, original)

    assert current == expected

def test_insert_pwa_scripts():
    from ghost_pwa.ghost_pwa import insert_pwa_scripts

    input_text = "{{ghost_head}}{{ghost_foot}}"

    with_header_and_footer = insert_pwa_scripts(input_text)

    assert "PWA" in with_header_and_footer

class ExampleArchiveWithOneSubdirAndSomeFiles(ExampleArchive):
    def _create_archive(self, name):
        with ZipFile(name, 'w') as out_zip:
            mkdir(out_zip, "Casper-3.1.3")
            default_hbs_content = "{{ghost_head}}{{ghost_foot}}"
            out_zip.writestr("Casper-3.1.3/default.hbs", default_hbs_content)
            out_zip.writestr(
                "Casper-3.1.3/readme",
                "Additional file - should be kept in the archive"
            )

def test_process_zip():
    with ExampleArchiveWithOneSubdirAndSomeFiles("test.zip") as input_zip:
        path = get_processed_zip(input_zip)
        assert "-PWA" in path.stem

        files_inside = ZipFile(path).namelist()
        assert "Casper-3.1.3/default.hbs" in files_inside
        assert "Casper-3.1.3/manifest.webmanifest" in files_inside
        assert "Casper-3.1.3/sw.js" in files_inside
        assert "Casper-3.1.3/assets/icons/ghost-2.png" in files_inside
        assert "Casper-3.1.3/assets/icons/ghost-4.png" in files_inside
        assert "Casper-3.1.3/readme" in files_inside
        assert files_inside.count("Casper-3.1.3/default.hbs") == 1

        default_hbs = ZipFile(path).read("Casper-3.1.3/default.hbs").decode()
        assert "PWA" in default_hbs

        manifest_content = ZipFile(path) \
            .read("Casper-3.1.3/manifest.webmanifest") \
            .decode()
        assert "fullscreen" in manifest_content

    remove("test-PWA.zip")
